locals {
 people = [
    { name = "Bob", age = 25 },
    { name = "Richard", age = 48 },
    { name = "Sally", age = 32 },
    { name = "Jennifer", age = NULL },
    { name = "Bob", age = 29 }
}


output "sorted_by_name" {
    value = sort(local.people, -1, "${upper(element(keys(local.people), count(local.people)))}")

locals {
  people = [
    { name = "Bob", age = 25 },
    { name = "Richard", age = 48 },
    { name = "Sally", age = 32 },
    { name = "Jennifer", age = NULL },
    { name = "Bob", age = 29 }
  ]

  filtered_people = [for p in local.people : p if p.age != null && p.age >= 30]
}

output "people_over_30" {
  value = local.filtered_people
}
